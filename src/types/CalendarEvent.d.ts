import { ColorTypes } from "@germix/germix-react-core";

export interface CalendarEvent
{
    start,
    title,
    color? : ColorTypes,
    payload?,
}
