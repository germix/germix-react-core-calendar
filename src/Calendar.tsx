import React from 'react';
import { Button } from '@germix/germix-react-core';
import { CalendarEvent } from './types/CalendarEvent';
import { Day } from './types/Day';

function chunk(array: Day[], chunkSize)
{
    var ret: Day[][] = [];
    for(var i = 0; i < array.length; i += chunkSize)
    {
        ret.push(array.slice(i, i + chunkSize));
    }
    return ret;
}

function generateDays(days: Day[], date, dayFrom, dayTo, monthType)
{
    const year = date.getFullYear();
    const month = date.getMonth();

    for(let i = dayFrom; i <= dayTo; i++)
    {
        days.push({
            day: i,
            month: monthType,
            date: (() =>
            {
                let d = ''+i;
                let m = ''+(month+1);

                if(m.length < 2) m = '0' + m;
                if(d.length < 2) d = '0' + d;

                return [year, m, d].join('-');
            })()
        })
    }
}

function getPrevMonth(date)
{
    let year = date.getFullYear();
    let month = date.getMonth();

    if(--month == 0)
    {
        year--;
        month = 12;
    }

    return new Date(year, month+1, 0);
}

function getNextMonth(date)
{
    let year = date.getFullYear();
    let month = date.getMonth();

    if(++month == 13)
    {
        year++;
        month = 1;
    }

    return new Date(year, month+1, 0);
}

function formateDate(date)
{
    let month = '' + (date.getMonth() + 1);
    let day = '' + date.getDate();
    let year = date.getFullYear();

    if(month.length < 2) 
        month = '0' + month;
    if(day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

interface Props
{
    labels: {
        months,
        header: {
            monday, tuesday, wednesday, thursday, friday, saturday, sunday
        }
    },
    month,
    events: CalendarEvent[],
    onChangeMonth(newMonth),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Calendar extends React.Component<Props,State>
{
    render()
    {
        const monthDate = new Date(this.props.month);

        const year = monthDate.getFullYear();

        console.log('year: ' + year);

        const month = monthDate.getMonth();
        
        console.log('month: ' + month);

        // Day of week
        const firstDayOfWeek = (new Date(year, month, 0)).getDay();

        console.log('firstDayOfWeek: ' + firstDayOfWeek);
        // Get days in month
        const daysInMonth = new Date(year, month+1, 0).getDate();

        console.log('daysInMonth: ' + daysInMonth);

        let days: Day[] = [];
        if(firstDayOfWeek !== 6)
        {
            let prevMonth = getPrevMonth(monthDate);
            let prevDaysInMonth = prevMonth.getDate();
            generateDays(days, prevMonth, prevDaysInMonth - firstDayOfWeek, prevDaysInMonth, 'old');
        }

        generateDays(days, monthDate, 1, daysInMonth, '');

        if(days.length < (6*7))
        {
            let nextMonth = getNextMonth(monthDate);
            generateDays(days, nextMonth, 1, (6*7)-days.length, 'new');
        }
        const weeks = chunk(days, 7);

        return (
<div className='calendar'>
    { this.renderToolbar(year, month) }
    { this.renderTable(weeks) }
</div>
        );
    }

    private renderToolbar = (year, month) =>
    {
        return (
<div className='calendar-toolbar mb-2'>
    <div className='calendar-toolbar-left'>
        { this.props.labels.months[month] } { year }
    </div>
    <div className='calendar-toolbar-right'>
        <Button color='secondary' onClick={(e) =>
        {
            this.toPrevMonth();
        }}>«</Button>
        <Button extraClasses='ml-1' color='secondary' onClick={(e) =>
        {
            this.toNextMonth();
        }}>»</Button>
    </div>
</div>
        )
    }

    private renderTable = (weeks: Day[][]) =>
    {
        const { events } = this.props;
    
        return (

<table className="calendar-table">
    <thead>
        <tr>
            <th>{this.props.labels.header.sunday}</th>
            <th>{this.props.labels.header.monday}</th>
            <th>{this.props.labels.header.tuesday}</th>
            <th>{this.props.labels.header.wednesday}</th>
            <th>{this.props.labels.header.thursday}</th>
            <th>{this.props.labels.header.friday}</th>
            <th>{this.props.labels.header.saturday}</th>
        </tr>
    </thead>
    <tbody>
        { weeks.map((week, weekIndex) =>
        {
            return (
                <tr key={weekIndex} className="calendar-row">
                    { week.map((day, index) =>
                    {
                        return (
                            <td key={index} className={"calendar-cell" + (day.day === '' ? ' x' : '') }>
                                <div className='calendar-cell-dummy'>
                                </div>
                                <div className="calendar-cell-content">
                                    <div className="day-number">
                                        {day.day}
                                    </div>
                                    <div className="events-container">
                                        { events.map((event, index) =>
                                        {
                                            let className = [
                                                'event',
                                                'event-color-' + (event.color || 'default')
                                            ].join(' ');
                                            if(event.start === day.date)
                                                return <div key={index} className={className}>{day.date}</div>;
                                            return null;
                                        })}
                                    </div>
                                </div>
                            </td>
                        );
                    })}
                </tr>
            );
        })}
    </tbody>
</table>
        )
    }

    private toPrevMonth = () =>
    {
        this.props.onChangeMonth(formateDate(getPrevMonth(new Date(this.props.month))));
    }

    private toNextMonth = () =>
    {
        this.props.onChangeMonth(formateDate(getNextMonth(new Date(this.props.month))));
    }
};
export default Calendar;
