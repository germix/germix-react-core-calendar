# Germix React Core - Calendar

## About

Germix react core calendar component

## Installation

```bash
npm install @germix/germix-react-core-calendar
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
